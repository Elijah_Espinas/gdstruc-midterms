#include <iostream>
#include <string>
#include <time.h>
#include "UnorderedArray.h"
#include "OrderedArray.h"

using namespace std;

void main()
{
	srand(time(NULL));
	string userChar;
	int userNumber, arraySize;
	cout << "Enter size of array: ";
	cin >> arraySize;

	UnorderedArray<int> unordered(arraySize);
	OrderedArray<int> ordered(arraySize);

	for (int i = 0; i < arraySize; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	while (true)
	{
		cout << endl;
		system("pause");
		system("cls");

		cout << "What would you like to do?" << endl;
		cout << "[A]- Remove Element      [B]- Search Element     [c]- Expand and generate random values" << endl;
		cin >> userChar;

		if (userChar == "a")
		{
			cout << "Input an index to remove: ";
			cin >> userNumber;
			cout << endl;
			unordered.remove(userNumber);
			ordered.remove(userNumber);

			cout << "Index Removed: " << endl;
			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
			cout << endl << "Ordered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";

		}
		else if (userChar == "b")
		{

			cout << "Generated array: " << endl;
			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
			cout << endl << "Ordered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";

			cout << "Enter number to search: ";
			cin >> userNumber;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(userNumber);
			if (result >= 0)
				cout << userNumber << " was found at index " << result << ".\n";
			else
				cout << userNumber << " not found." << endl;
			
			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(userNumber);
			if (result >= 0)
				cout << userNumber << " was found at index " << result << ".\n";
			else
				cout << userNumber << " not found." << endl;

		}
		else if (userChar == "c")
		{
			cout << "Input the size of the expansion: ";
			cin >> userNumber;
			cout << endl;
			
			for (int i = 0; i < userNumber; i++)
			{
				int rng = rand() % 100+1;
				unordered.push(rng);
				ordered.push(rng);
			}
			cout << "Arrays now expanded!" << endl;
			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
			cout << endl << "Ordered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";

		}
	}
}